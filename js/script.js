function createHTML() {
  const div = document.querySelector(".ip-item");
  div.insertAdjacentHTML(
    "beforeend",
    `<h1 class="tittle">Я тебе знайду по IP</h1>
    <button class="ip-button">ТИЦ</button>`
  );
}

async function clickBtn() {
  createHTML();
  const btn = document.querySelector(".ip-button");
  const findIp = "https://api.ipify.org/?format=json"

  btn.addEventListener("click", async () => {
    const response = await fetch(findIp);
    const data = await response.json();
    console.log(data);
    const str = JSON.stringify(data.ip).replace(/"/g, "");

    const ipInfo = 'http://ip-api.com/json?fields=continent,country,region,city,district'

    const resultRes = await fetch(ipInfo);
    const ipRes = await resultRes.json();
    console.log(ipRes);

    const divInfo = document.querySelector(".ip-info");
    divInfo.insertAdjacentHTML("beforeend",
    `<ul class="ip-list">
    <p class="ip_list_item">Континент: ${ipRes.continent}</p>
    <p class="ip_list_item">Країна: ${ipRes.country}</p>
    <p class="ip_list_item">Місто: ${ipRes.city}</p>
    <p class="ip_list_item">Регіон: ${ipRes.region}</p>
    <p class="ip_list_item">Район: ${ipRes.district}Немає на сервері :(</p>
    </ul>`
    );
  });
}
clickBtn();
